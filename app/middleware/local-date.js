// const utc_env = process.env.UTC
const config = require('config')
// Function for Date Local Jakarta
const localDate = (string) => {
  var utc = new Date();

  //return utc.setHours(utc.getHours() + 7);
  // return utc.setHours(parseFloat(utc.getHours()) + parseFloat(utc_env));
  return utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc")));
};
// Exporting Function
module.exports = localDate;
