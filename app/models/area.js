const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

const AreaSchema = new mongoose.Schema({    
    ar_id : {
        type: Number,
        required: true,
    },
    bd_id : {
        type: Number,
        required: true,
    },
    ar_code : {
        type: String,
        required: true,
    },
    ar_name : {
        type: String,
        require: true,
    },
    ar_floor : {
        type: String,
    },
    ar_position : {
        type: String,
    },
    active : {
        type: Number,
        default: 1,
    },
    create_time : {
        type: Date,
        default: localDate,
    },
    update_time : {
        type: Date,
    }
})

const Area = mongoose.model("Area", AreaSchema);
module.exports = Area;