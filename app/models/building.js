const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

const BuildingSchema = new mongoose.Schema({
    bd_id : {
        type: Number,
        required: true,
    },
    cs_id : {
        type: Number,
        required: true,
    },
    bd_code : {
        type: String,
        required: true,
    },
    bd_name : {
        type: String,
        require: true,
    },
    bd_city : {
        type: String,
    },
    bd_address : {
        type: String,
    },
    active : {
        type: Number,
        default: 1,
    },
    create_time : {
        type: Date,
        default: localDate,
    },
    update_time : {
        type: Date,
    }
})

const Building = mongoose.model("Building", BuildingSchema);
module.exports = Building;