const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

const CustomerSchema = new mongoose.Schema({
    cs_id : {
        type: Number,
        required: true,
    },
    cs_name : {
        type: String,
        require: true,
    },
    cs_phone : {
        type: String,
    },
    cs_address : {
        type: String,
    },
    active : {
        type: Number,
        default: 1,
    },
    create_time : {
        type: Date,
        default: localDate,
    },
    update_time : {
        type: Date,
    }
})

const Customer = mongoose.model("Customer", CustomerSchema);
module.exports = Customer;