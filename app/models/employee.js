const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

var SchemaTypes = mongoose.Schema.Types;
const EmployeeSchema = new mongoose.Schema({
    em_id: {
        type: Number,
        required: true,
    },
    dp_id: {
        type: Number,
        required: true,
    },
    em_name: {
        type: String,
        required: true,
    },
    em_gender: {
        type: String,
        required: true,
    },
    em_dob: {
        type: String,
        required: true,
    },
    em_pob: {
        type: String,
        required: true,
    },
    em_address: {
        type: String,
        // required: true,
    },
    active: {
        type: Number,
        default: 1,
    },
    create_time: {
        type: Date,
        default: localDate
    },
    update_time: {
        type: Date,
    }
});

const Employee = mongoose.model("Employee", EmployeeSchema);
module.exports = Employee;

// const employee1 = new Employee({
//     em_id       : 1,
//     dp_id       : 2,
//     em_name     : 'Sebastian Chandra',
//     em_gender   : 'L',
//     em_dob      : '2021-09-23',
//     em_pob      : 'Jakarta',
//     em_address  : 'Bekasi Utara'
// })

// employee1.save().then((employee) =>  console.log(employee))