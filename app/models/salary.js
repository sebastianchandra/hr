const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

var SchemaTypes = mongoose.Schema.Types;
const SalarySchema = new mongoose.Schema({
    sa_id: {
        type: Number,
        required: true,
    },
    em_id: {
        type: Number,
        required: true,
    },   
    cs_id: {
        type: Number,
        required: true,
    },   
    cs_level: {
        type: String,
        required: true,
    },
    cs_badge: {
        type: String,
        required: true,
    },
    salary: {
        type: String,
        required: true,
    },
    active: {
        type: Number,
        default: 1,
    },
    create_time: {
        type: Date,
        default: localDate
    },
    update_time: {
        type: Date,
    }
});

const Salary = mongoose.model("Salary", SalarySchema);
module.exports = Salary;