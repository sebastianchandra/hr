const mongoose = require("mongoose");
const localDate = require("../middleware/local-date");
require('@mongoosejs/double');

var SchemaTypes = mongoose.Schema.Types;
const UsersSchema = new mongoose.Schema({

    user_id: {
        type: Number,
        required: true,
    },
    user_role: {
        type: Number,
        required: true,
    },
    user_level: {
        type: Number,
        required: true,
    },
    user_phone: {
        type: String,
        required: true,
    },
    user_email: {
        type: String,
        required: true,
        lowercase: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    active: {
        type: Number,
        default: 1,
    },
    create_time: {
        type: Date,
        default: localDate
    },
    update_time: {
        type: Date,
    }
});

const Users = mongoose.model("Users", UsersSchema);
module.exports = Users;

// const employee1 = new Employee({
//     em_id       : 1,
//     dp_id       : 2,
//     em_name     : 'Sebastian Chandra',
//     em_gender   : 'L',
//     em_dob      : '2021-09-23',
//     em_pob      : 'Jakarta',
//     em_address  : 'Bekasi Utara'
// })

// employee1.save().then((employee) =>  console.log(employee))