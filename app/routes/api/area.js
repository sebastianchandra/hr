const express = require('express')
const http = require('http');
const momentjs = require("moment");
const config = require("config");
const router = express.Router()    
const { body, validationResult } = require("express-validator");
const Building = require('../../models/building')
const Area = require('../../models/area')
const flash = require('connect-flash')

router.get('/', async (req,res) => {
  // const area = await Area.find({"active" : 1}).sort({_id: -1})
  let search = [
    {
      '$lookup': {
        'from': 'buildings', 
        'localField': 'bd_id', 
        'foreignField': 'bd_id', 
        'as': 'fromItems'
      }
    }, {
      '$replaceRoot': {
        'newRoot': {
          'newRoot': {
            '$mergeObjects': [
              {
                '$arrayElemAt': [
                  '$fromItems', 0
                ]
              }, '$$ROOT'
            ]
          }
        }
      }
    }, {
      '$project': {
        'fromItems': 0
      }
    }
  ]

  const area = await Area.aggregate(search)
  
  console.log(area)
  
  res.render('../app/views/masters/areaView', {
    layout  : '../app/views/body',
    title   : 'View Area',
    area,
    url     : http,
    msg     : req.flash('msg'),
  })

})

router.get('/input', async (req,res) => {
  const building = await Building.find({"active" : 1}).sort({_id: -1})

  res.render('../app/views/masters/areaInput', {
    layout  : '../app/views/body',
    title   : 'Input Area',
    building,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/act_area', async (req,res) => {
  // console.log(req.body)
  try {
    let dataArea    = await Area.findOne().sort({ar_id:-1}).limit(1).lean();    
    // console.log(dataArea)
    if(!req.body.ar_id){
      if(dataArea){
        var ar_id       = dataArea.ar_id+1
      }else{
        var ar_id       = 1
      }
    }else{
      ar_id             = req.body.ar_id
    }

    let dataInsert      = {
      ar_id : ar_id,
      bd_id : req.body.bd_id,
      ar_code : req.body.ar_code,
      ar_name  : req.body.ar_name,
      ar_floor : req.body.ar_floor,
      ar_position : req.body.ar_position
    }

    if(req.body.ar_id){
      const updateAct = Area.updateOne(
        {
          ar_id : req.body.ar_id
        }, { 
          $set : dataInsert
        }
      )
      updateAct.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error)
      })
      res.json({status: "success", message: "Data has been Update", data: dataInsert})
    }else{
      Area.insertMany(dataInsert)
      res.json({status: "success", message: "Data has been Save", data: dataInsert})
    }

  } catch (err) {
    console.log(err.message);
    res.status(500).json({ status: "failed", message: " Server error : " + err.message, data: [] });
  }   

})

router.get('/edit/:id', async (req,res) => {
  const ar_id   = req.params.id
  const detArea   = await Area.findOne({ar_id : ar_id})
  const building = await Building.find({"active" : 1}).sort({_id: -1})
  // console.log(detBuilding)

  res.render('../app/views/masters/areaEdit', {
    layout  : '../app/views/body',
    title   : 'Edit Area',
    building,
    area    : detArea,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/del_detail', async (req,res) => {
  try {
    var utc = new Date();
    var day = momentjs(utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc"))));
    const ar_id       = req.body.id;
    const area    = await Area.findOne({ em_id: ar_id });
    if (area) {
      let dataInsert      = {
        active : 0,
        update_time : day,
      }

      await Area.updateOne(
        {
          ar_id : ar_id
        }, { 
          $set : dataInsert
        }
      )

      return res.json({status: "success", message: "Data has been deleted!", data: []});
    }
    res.status(400).json({ status: "failed", message: "Tidak ada bank untuk ID tersebut", data:[] });

  } catch (err) {
    console.log(err)
  }  

})

module.exports = router;