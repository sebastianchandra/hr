const express = require('express')
const http = require('http');
const momentjs = require("moment");
const config = require("config");
const router = express.Router()    
const { body, validationResult } = require("express-validator");
const Building = require('../../models/building')
const Customer = require('../../models/customer')
const flash = require('connect-flash')

router.get('/', async (req,res) => {
  let search = [
    {
      '$lookup': {
        'from': 'customers', 
        'localField': 'cs_id', 
        'foreignField': 'cs_id', 
        'as': 'fromItems'
      }
    }, {
      '$replaceRoot': {
        'newRoot': {
          'newRoot': {
            '$mergeObjects': [
              {
                '$arrayElemAt': [
                  '$fromItems', 0
                ]
              }, '$$ROOT'
            ]
          }
        }
      }
    }, {
      '$project': {
        'fromItems': 0
      }
    }
  ]
  //const building = await Building.find({"active" : 1}).sort({_id: -1})
  const building = await Building.aggregate(search) 
  // console.log(building)
  
  res.render('../app/views/masters/buildingView', {
    layout  : '../app/views/body',
    title   : 'View Building',
    building,
    url     : http,
    msg     : req.flash('msg'),
  })

})

router.get('/input', async (req,res) => {
  const customer = await Customer.find({"active" : 1}).sort({_id: -1})

  res.render('../app/views/masters/buildingInput', {
    layout  : '../app/views/body',
    title   : 'Input Building',
    customer,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/act_building', async (req,res) => {
  // console.log(req.body)
  try {
    let dataBuilding    = await Building.findOne().sort({bd_id:-1}).limit(1).lean();    
    console.log(dataBuilding)
    if(!req.body.bd_id){
      if(dataBuilding){
        var bd_id       = dataBuilding.bd_id+1
      }else{
        var bd_id       = 1
      }
    }else{
      bd_id             = req.body.bd_id
    }

    let dataInsert      = {
                              bd_id : bd_id,
                              cs_id : req.body.cs_id,
                              bd_code : req.body.bd_code,
                              bd_name  : req.body.bd_name,
                              bd_city : req.body.bd_city,
                              bd_address : req.body.bd_address
                          }

    if(req.body.bd_id){
      const updateAct = Building.updateOne(
        {
          bd_id : req.body.bd_id
        }, { 
          $set : dataInsert
        }
      )
      updateAct.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error)
      })
      res.json({status: "success", message: "Data has been Update", data: dataInsert})
    }else{
      Building.insertMany(dataInsert)
      res.json({status: "success", message: "Data has been Save", data: dataInsert})
    }

  } catch (err) {
    console.log(err.message);
    res.status(500).json({ status: "failed", message: " Server error : " + err.message, data: [] });
  }   

})

router.get('/edit/:id', async (req,res) => {
  const bd_id   = req.params.id
  const detBuilding   = await Building.findOne({bd_id : bd_id})
  const customer = await Customer.find({"active" : 1}).sort({_id: -1})
  console.log(detBuilding)

  res.render('../app/views/masters/buildingEdit', {
    layout  : '../app/views/body',
    title   : 'Edit Building',
    customer,
    building: detBuilding,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/del_detail', async (req,res) => {
  try {
    var utc = new Date();
    var day = momentjs(utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc"))));
    const bd_id       = req.body.id;
    const building    = await Building.findOne({ em_id: bd_id });
    if (building) {
      let dataInsert      = {
        active : 0,
        update_time : day,
      }

      await Building.updateOne(
        {
          bd_id : bd_id
        }, { 
          $set : dataInsert
        }
      )

      return res.json({status: "success", message: "Data has been deleted!", data: []});
    }
    res.status(400).json({ status: "failed", message: "Tidak ada bank untuk ID tersebut", data:[] });

  } catch (err) {
    console.log(err)
  }  

})

module.exports = router;