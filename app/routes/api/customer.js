const express = require('express')
const http = require('http');
const momentjs = require("moment");
const config = require("config");
const router = express.Router()    
const { body, validationResult } = require("express-validator");
const Customer = require('../../models/customer')
const flash = require('connect-flash')

router.get('/', async (req,res) => {
  const customer = await Customer.find({"active" : 1}).sort({_id: -1})
  
  // console.log(http)
  
  res.render('../app/views/masters/customerView', {
    layout  : '../app/views/body',
    title   : 'View Customer',
    customer,
    url     : http,
    msg     : req.flash('msg'),
  })

})

router.get('/input', async (req,res) => {
  
  res.render('../app/views/masters/customerInput', {
    layout  : '../app/views/body',
    title   : 'Input Customer',
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/act_customer', async (req,res) => {
  // console.log(req.body)
  try {
    let dataCustomer    = await Customer.findOne().sort({cs_id:-1}).limit(1).lean();    

    if(!req.body.cs_id){
      if(dataCustomer){
        var cs_id       = dataCustomer.cs_id+1
      }else{
        var cs_id       = 1
      }
    }else{
      cs_id             = req.body.cs_id
    }

    let dataInsert      = {
                              cs_id : cs_id,
                              cs_name  : req.body.cs_name,
                              cs_phone : req.body.cs_phone,
                              cs_address : req.body.cs_address
                          }

    if(req.body.cs_id){
      const updateAct = Customer.updateOne(
        {
          cs_id : req.body.cs_id
        }, { 
          $set : dataInsert
        }
      )
      updateAct.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error)
      })
      res.json({status: "success", message: "Data has been Update", data: dataInsert})
    }else{
      Customer.insertMany(dataInsert)
      res.json({status: "success", message: "Data has been Save", data: dataInsert})
    }

  } catch (err) {
    console.log(err.message);
    res.status(500).json({ status: "failed", message: " Server error : " + err.message, data: [] });
  }   

})

router.get('/edit/:id', async (req,res) => {
  const id_customer   = req.params.id
  const detCustomer   = await Customer.findOne({cs_id : id_customer})
  console.log(detCustomer)

  res.render('../app/views/masters/customerEdit', {
    layout  : '../app/views/body',
    title   : 'Edit Customer',
    customer: detCustomer,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/del_detail', async (req,res) => {
  try {
    var utc = new Date();
    var day = momentjs(utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc"))));
    const cs_id       = req.body.id;
    const customer    = await Customer.findOne({ em_id: cs_id });
    if (customer) {
      let dataInsert      = {
        active : 0,
        update_time : day,
      }

      await Customer.updateOne(
        {
          cs_id : cs_id
        }, { 
          $set : dataInsert
        }
      )

      return res.json({status: "success", message: "Data has been deleted!", data: []});
    }
    res.status(400).json({ status: "failed", message: "Tidak ada bank untuk ID tersebut", data:[] });

  } catch (err) {
    console.log(err)
  }  

})

module.exports = router;