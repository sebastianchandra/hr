const express = require('express')
const http = require('http');
const momentjs = require("moment");
const config = require("config");
const router = express.Router()    
const { body, validationResult } = require("express-validator");
const Employee = require('../../models/employee')
const flash = require('connect-flash')

router.get('/', async (req,res) => {
  const employee = await Employee.find({"active" : 1}).sort({_id: -1})
  
  // console.log(http)
  
  res.render('../app/views/masters/employeeView', {
    layout  : '../app/views/body',
    title   : 'View Employee',
    employee,
    url     : http,
    msg     : req.flash('msg'),
  })

})

router.get('/input', async (req,res) => {
  
  res.render('../app/views/masters/employeeInput', {
    layout  : '../app/views/body',
    title   : 'Input Employee',
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/act_employee', async (req,res) => {
  // console.log(req.body)
  try {
    let dataEmployee    = await Employee.findOne().sort({em_id:-1}).limit(1).lean();    

    if(!req.body.em_id){
      if(dataEmployee){
        var em_id       = dataEmployee.em_id+1
      }else{
        var em_id       = 1
      }
    }else{
      em_id             = req.body.em_id
    }

    let dataInsert      = {
                              em_id : em_id,
                              dp_id : req.body.dp_id,
                              em_name  : req.body.em_name,
                              em_gender : req.body.em_gender,
                              em_dob : req.body.em_dob,
                              em_pob : req.body.em_pob,
                              em_address : req.body.em_address
                          }

    if(req.body.em_id){
      const updateAct = Employee.updateOne(
        {
          em_id : req.body.em_id
        }, { 
          $set : dataInsert
        }
      )
      updateAct.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error)
      })
      res.json({status: "success", message: "Data has been Update", data: dataInsert})
    }else{
      Employee.insertMany(dataInsert)
      res.json({status: "success", message: "Data has been Save", data: dataInsert})
    }

  } catch (err) {
    console.log(err.message);
    res.status(500).json({ status: "failed", message: " Server error : " + err.message, data: [] });
  }   

})

router.get('/edit/:id', async (req,res) => {
  const id_employee   = req.params.id
  const detEmployee   = await Employee.findOne({em_id : id_employee})
  console.log(detEmployee)

  res.render('../app/views/masters/employeeEdit', {
    layout  : '../app/views/body',
    title   : 'Edit Employee',
    employee: detEmployee,
    dept    : [
      {
        dp_id     : '1',
        dp_code   : 'PR',
        dp_name   : 'Purchasing'
      },
      {
        dp_id     : '2',
        dp_code   : 'IT',
        dp_name   : 'IT'
      },
      {
        dp_id     : '3',
        dp_code   : 'FN',
        dp_name   : 'Finance'
      },
    ],
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/del_detail', async (req,res) => {
  try {
    var utc = new Date();
    var day = momentjs(utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc"))));
    const em_id       = req.body.id;
    const employee    = await Employee.findOne({ em_id: em_id });
    if (employee) {
      let dataInsert      = {
        active : 0,
        update_time : day,
      }

      await Employee.updateOne(
        {
          em_id : em_id
        }, { 
          $set : dataInsert
        }
      )

      return res.json({status: "success", message: "Data has been deleted!", data: []});
    }
    res.status(400).json({ status: "failed", message: "Tidak ada bank untuk ID tersebut", data:[] });

  } catch (err) {
    console.log(err)
  }
  

})

module.exports = router;