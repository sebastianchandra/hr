const express = require('express')
const http = require('http');
const momentjs = require("moment");
const config = require("config");
const router = express.Router()    
const { body, validationResult } = require("express-validator");
const Employee = require('../../models/employee')
const Salary = require('../../models/salary')
const Customer = require('../../models/customer')
const flash = require('connect-flash')

router.get('/', async (req,res) => {
  // const salary = await Salary.find({"active" : 1}).sort({_id: -1})
  let search = [
    {
      '$lookup': {
        'from': 'employee', 
        'localField': 'em_id', 
        'foreignField': 'em_id', 
        'as': 'fromItems'
      }
    }, {
      '$replaceRoot': {
        'newRoot': {
          'newRoot': {
            '$mergeObjects': [
              {
                '$arrayElemAt': [
                  '$fromItems', 1
                ]
              }, '$$ROOT'
            ]
          }
        }
      }
    }, {
      '$project': {
        'fromItems': 0
      }
    }
  ]

  const salary = await Salary.aggregate(search) 
  console.log(salary)
  res.render('../app/views/masters/salaryView', {
    layout  : '../app/views/body',
    title   : 'View Salary',
    salary,
    url     : http,
    msg     : req.flash('msg'),
  })

})

router.get('/input', async (req,res) => {
  const employee = await Employee.find({"active" : 1}).sort({_id: -1})
  const customer = await Customer.find({"active" : 1}).sort({_id: -1})
  
  res.render('../app/views/masters/salaryInput', {
    layout  : '../app/views/body',
    title   : 'Input Salary',
    employee,
    customer,
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/act_employee', async (req,res) => {
  // console.log(req.body)
  try {
    let dataSalary    = await Salary.findOne().sort({em_id:-1}).limit(1).lean();    

    if(!req.body.sa_id){
      if(dataSalary){
        var sa_id       = dataSalary.sa_id+1
      }else{
        var sa_id       = 1
      }
    }else{
      sa_id             = req.body.sa_id
    }

    let dataInsert      = {
      sa_id : sa_id,
      em_id : req.body.em_id,
      cs_id  : req.body.cs_id,
      cs_level : req.body.cs_level,
      cs_badge : req.body.cs_badge,
      salary : req.body.salary
    }

    if(req.body.sa_id){
      const updateAct = Salary.updateOne(
        {
          sa_id : req.body.sa_id
        }, { 
          $set : dataInsert
        }
      )
      updateAct.then((result) => {
        console.log(result);
      }).catch((error) => {
        console.log(error)
      })
      res.json({status: "success", message: "Data has been Update", data: dataInsert})
    }else{
      Salary.insertMany(dataInsert)
      res.json({status: "success", message: "Data has been Save", data: dataInsert})
    }

  } catch (err) {
    console.log(err.message);
    res.status(500).json({ status: "failed", message: " Server error : " + err.message, data: [] });
  }   

})

router.get('/edit/:id', async (req,res) => {
  const id_employee   = req.params.id
  const detEmployee   = await Employee.findOne({em_id : id_employee})
  console.log(detEmployee)

  res.render('../app/views/masters/employeeEdit', {
    layout  : '../app/views/body',
    title   : 'Edit Employee',
    employee: detEmployee,
    dept    : [
      {
        dp_id     : '1',
        dp_code   : 'PR',
        dp_name   : 'Purchasing'
      },
      {
        dp_id     : '2',
        dp_code   : 'IT',
        dp_name   : 'IT'
      },
      {
        dp_id     : '3',
        dp_code   : 'FN',
        dp_name   : 'Finance'
      },
    ],
    url     : req.get('host'),
    msg     : req.flash('msg'),
  })

})

router.post('/del_detail', async (req,res) => {
  try {
    var utc = new Date();
    var day = momentjs(utc.setHours(parseFloat(utc.getHours()) + parseFloat(config.get("utc"))));
    const em_id       = req.body.id;
    const employee    = await Employee.findOne({ em_id: em_id });
    if (employee) {
      let dataInsert      = {
        active : 0,
        update_time : day,
      }

      await Employee.updateOne(
        {
          em_id : em_id
        }, { 
          $set : dataInsert
        }
      )

      return res.json({status: "success", message: "Data has been deleted!", data: []});
    }
    res.status(400).json({ status: "failed", message: "Tidak ada bank untuk ID tersebut", data:[] });

  } catch (err) {
    console.log(err)
  }
  

})

module.exports = router;