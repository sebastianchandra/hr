const express = require("express");
const router = express.Router();

router.get("/", (req,res) => {
    res.send("success")
})

// Master Employee
router.use("/employee", require("./api/employee"));
router.use("/employee/input", require("./api/employee"));
router.use("/employee/edit", require("./api/employee"));
router.use("/employee/act_employee", require("./api/employee"));

// Master Customer
router.use("/customer", require("./api/customer"));
router.use("/customer/input", require("./api/customer"));
router.use("/customer/edit", require("./api/customer"));
router.use("/customer/act_employee", require("./api/customer"));

// Master Building
router.use("/building", require("./api/building"));
router.use("/building/input", require("./api/building"));
router.use("/building/edit", require("./api/building"));
router.use("/building/act_employee", require("./api/building"));

// Master Area
router.use("/area", require("./api/area"));
router.use("/area/input", require("./api/area"));
router.use("/area/edit", require("./api/area"));
router.use("/area/act_employee", require("./api/area"));

// Master Salary
router.use("/salary", require("./api/salary"));
router.use("/salary/input", require("./api/salary"));
router.use("/salary/edit", require("./api/salary"));
router.use("/salary/act_employee", require("./api/salary"));

// Master Users
router.use("/users", require("./api/Users"));
router.use("/Users/input", require("./api/Users"));
router.use("/Users/edit", require("./api/Users"));
router.use("/Users/act_employee", require("./api/Users"));

module.exports = router;