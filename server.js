const express = require('express')
const expressLayouts = require('express-ejs-layouts')
const app = express()
const port = 3000

const session = require('express-session')
const cookieParser = require('cookie-parser')
const flash = require('connect-flash')

process.env.NODE_CONFIG_DIR = __dirname + "/app/config/"
// Database
const mongoDB = require("./app/config/db") // MongoDB
// Connect MongoDB
mongoDB()


// Gunakan EJS
app.set('view engine','ejs')
app.use(expressLayouts)
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.use(cookieParser('secret'))
app.use(
  session({
    cookie  : {maxAge: 6000 },
    secret  : 'secret',
    resave  : true,
    saveUninitialized: true,
  })
)
app.use(flash())

// app.get('/', (req, res) => {
//   res.render('../app/views/layouts/dashboard', {
//     layout: '../app/views/body',
//     title: 'Dashboard'
//   });
// })

// app.get('/', (req, res) => {
//   res.render('', {
//     layout: 'layouts/login'
//   });
// })

app.use("/", require("./app/routes"))

app.listen(port, () => {
  console.log(`Mongo Test | Example app listening at http://localhost:${port}`)
})



// app.get('/about', (req, res) => {
//   res.render('about', {
//     layout: 'layouts/body',
//     title: 'Halaman About'
//   });
// })

// app.get('/contact', (req, res) => {
//   res.render('contact', {
//     layout: 'layouts/body',
//     title: 'Halaman Contact'
//   });
// })



